import json
import sys


def secret_qg(file):
    with open(file, 'r', encoding='utf-8') as file:
        data = json.load(file)
        count = sum(1 for item in data if 'Description' in item)
        return count

def sast_qg(file):
    with open(file, 'r') as f:
        data = json.load(f)
        counter = 0
        for result in data.get('results', []):
            impact = result.get('extra', {}).get('metadata', {}).get('impact')
            if impact == 'HIGH' or impact == 'MEDIUM':
                counter += 1
        return counter

def sca_qg(file):
    with open(file, 'r') as f:
        data = json.load(f)
        counter = 0
        for dependency in data.get('dependencies', []):
            if 'vulnerabilities' in dependency:
                for vuln in data.get('vulnerabilities', []):
                    counter += 1
        return counter

sast = sast_qg('semgrep-report.json')
ss = secret_qg('gitleaks-report.json')
sca = sca_qg('dependency-check-report.json')

if (sast > 30) or (ss > 5) or (sca > 100):
    print(f"Bad QG: sast:{sast}, ss:{ss}, sca:{sca}")
    sys.exit(1)
else:
    print(f"Good QG")
    sys.exit(0)
